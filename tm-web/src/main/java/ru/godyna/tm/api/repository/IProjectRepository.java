package ru.godyna.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.godyna.tm.model.Project;
@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {
}
